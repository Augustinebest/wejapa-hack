import React, { useEffect, useState } from 'react';
import './styles/dashboard.css';
import Axios from 'axios';
import { URL } from '../utils/config';

const Dashboard = (props) => {
    const [jobs, setJObs] = useState([]);
    const profile = props.location.state.data.developer
    useEffect(() => {
        const token = localStorage.getItem('user')
        Axios.get(`${URL}/job/all`, {
            headers: {
                "Authorization": token
            }
        }).then(res => {
            if (res.data.success) {
                setJObs(res.data.data.jobs)
            }
        })
            .catch(err => {
                console.log(err);
            })
    }, [])

    const getAJob = (id) => {
        props.history.push({pathname: '/job', state: id})
    }
    console.log(profile)
    return (
        <div className='dashboard_container'>
            <div className='prof_details'>
                <div>Hello {profile.name}</div>
                <div>Logged in as: {profile.email}</div>
            </div>
            <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                <div className='alljobs'>

                    <div style={{ width: '75%' }}>
                        {
                            jobs && jobs.length > 0 ?
                                jobs.map((job, index) => {
                                    return (
                                        <div className='ajob' key={index} onClick={() => getAJob(job._id)}>
                                            <div className='title'>{job.title} - <span className='status'>{job.status}</span></div>
                                            <div className='description'>
                                                {job.description}
                                        </div>
                                            <div className='temp_'>
                                                <div className='type'>{job.type}</div>
                                                <div className='location'>{job.location === 'Remote' ? '' : job.location}</div>
                                                <div className='salary'>${job.salary}</div>
                                            </div>
                                        </div>
                                    )
                                })
                                : <div>Loading...</div>
                        }
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Dashboard;