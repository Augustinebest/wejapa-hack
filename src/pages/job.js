import React, { useEffect, useState } from 'react';
import Axios from 'axios';
import { URL } from '../utils/config';
import './styles/dashboard.css'

const Job = (props) => {
    const jobID = props.location.state;
    const [job, setJob] = useState(null)

    useEffect(() => {
        const token = localStorage.getItem('user');
        Axios.get(`${URL}/job/${jobID}`, {
            headers: {
                "Authorization": token
            }
        }).then(res => {
            if (res.data.success) {
                setJob(res.data.data)
            }
        })
            .catch(err => {
                console.log(err)
            })
    }, [])

    console.log(job)
    return (
        <div className='job_page'>
            {
                job ?
                    <div style={{ width: '50%', border: '1px solid #eee', padding: '20px', borderRadius: '5px' }}>
                        <div style={{ fontWeight: "bold", marginBottom: '10px' }}>{job && job.title} - <span style={{ fontWeight: 'normal', fontSize: '12px' }}>{job && job.status}</span></div>
                        <div style={{ fontSize: '13px', marginBottom: '10px' }}>{job && job.description}</div>
                        <div style={{ display: 'flex', justifyContent: 'space-between', marginBottom: '10px' }}>
                            <div>{job && job.experience}</div>
                            <div>{job && job.location}</div>
                            <div>${job && job.salary}</div>
                        </div>
                        <div style={{ display: 'flex', justifyContent: 'space-between', marginBottom: '10px' }}>
                            <div>{job && job.type}</div>
                            <div>work rate: {job && job.hoursPerWeek}</div>
                        </div>
                        <div>Benefits</div>
                        <ul>
                            {job && job.benefits ?
                                job.benefits.map((be, index) => {
                                    return (
                                        <li key={index}>{be}</li>
                                    )
                                })
                                : <li>No benefit</li>
                            }
                        </ul>
                    </div>
                    : <div>loading...</div>
            }
        </div>
    )
}

export default Job;