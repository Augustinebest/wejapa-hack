import React, { useState } from 'react';
import './styles/login.css'
import logo from '../assets/myweja.png';
import Axios from 'axios';
import { URL } from '../utils/config';

const Login = (props) => {

    const [inputs, setInputs] = useState({ email: '', password: '' })
    const [err, setErr] = useState({ success: false, failure: false, message: null, type: "" })
    const [ loading, setLoading ] = useState(false)

    const handleChange = (e) => {
        setInputs({ ...inputs, [e.target.name]: e.target.value })
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        setLoading(true)
        const { email, password } = inputs;
        const payload = {
            email, password
        }
        Axios.post(`${URL}/developer/login`, payload, {
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(res => {
            setLoading(false)
            if (res.data.success) {
                setErr({...err, success: true, message: res.data.message, type: "alert-success"})
                localStorage.setItem('user', res.data.data.token)
                setTimeout(() => {
                    props.history.push({pathname: '/dashboard', state: res.data})
                }, 1000)
            } else {
                setLoading(false)
                setErr({...err, failure: true, message: res.data.message, type: "alert-danger"})
            }
        })
            .catch(err => {
                setLoading(false)
                setErr({...err, failure: true, message: err.response.data.message, type: "alert-danger"})
            })

    }

    return (
        <div className='my_container'>
            <div className='side'>
                <div style={{ textAlign: 'center' }}>
                    <img src={logo} style={{ width: '100px', height: '100px' }} />
                    <div style={{ fontWeight: 'bold', marginTop: '20px', fontSize: '20px' }}>WeJapa</div>
                </div>
            </div>
            <div className='loginsection'>
                <div className='login_frame'>
                    <div style={{ textAlign: 'center', margin: '30px 0px' }}>Welcome back</div>
                    {
                        err.success || err.failure ?
                        <div style={{ justifyContent: 'center', alignItems: 'center', display: 'flex'}}>
                        <div style={{ width: '85%', justifyContent: 'center' }} className={"alert alert-dismissible fade show " + err.type} role="alert">
                            <div>{err.message}</div>
                            <button type="button" className="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                        : null
                    }

                    <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                        <form onSubmit={handleSubmit}>
                            <input type='email' placeholder='email' name='email' value={inputs.email} onChange={handleChange} required />
                            <input type='password' placeholder='password' name='password' value={inputs.password} onChange={handleChange} required />
                            <input type='submit' value={loading ? 'LOADING...' : 'LOGIN'} />
                        </form>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Login;