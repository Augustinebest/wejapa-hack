import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Login from './pages/login';
import Homepage from './pages/homepage';
import Dashboard from './pages/dashboard';
import Job from './pages/job';

const Router = () => {
    return (
        <BrowserRouter>
            <Switch>
                <Route exact path='/' component={Homepage} />
                <Route exact path='/login' component={Login} />
                <Route exact path='/dashboard' component={Dashboard} />
                <Route exact path='/job' component={Job} />
            </Switch>
        </BrowserRouter>
    )
}

export default Router;